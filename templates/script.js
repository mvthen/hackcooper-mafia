      $(document).ready(function(){
        $("#link").attr('value',document.URL);
      })
      TB.addEventListener("exception", exceptionHandler);
        var session = TB.initSession("{{session_id}}"); // Replace with your own session ID. See https://dashboard.tokbox.com/projects
        session.addEventListener("sessionConnected", sessionConnectedHandler);
        session.addEventListener("streamCreated", streamCreatedHandler);
        session.connect({{api_key}}, "{{token}}"); // Replace with your API key and token. See https://dashboard.tokbox.com/projects

        function sessionConnectedHandler(event) {
         subscribeToStreams(event.streams);
         session.publish();
       }

       function streamCreatedHandler(event) {
        subscribeToStreams(event.streams);
      }

      function subscribeToStreams(streams) {
        for (var i = 0; i < streams.length; i++) {
          var stream = streams[i];
          console.log(stream.connection.connectionId+"&"+session.connection.connectionId);
          if ((stream.connection.connectionId != session.connection.connectionId)) {
            session.subscribe(stream);
          }
        }
      }

      function exceptionHandler(event) {
        alert(event.message);
      }
      