from flask import Flask, redirect, url_for, render_template
import OpenTokSDK
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

app = Flask(__name__)

app.config.from_object(__name__)
#app.config["DEBUG"] = True

api_key = "44709922" # Replace with your OpenTok API key.
api_secret = "a234033336c54484833603f30c37267ccd607c66" # Replace with your OpenTok API secret.

opentok_sdk = OpenTokSDK.OpenTokSDK(api_key, api_secret)

@app.route('/')
def index():
	session_properties = {OpenTokSDK.SessionProperties.p2p_preference: "disabled"}
	session = opentok_sdk.create_session(None, session_properties)
	url= url_for('chat',session_id=session.session_id)
	return redirect(url)

@app.route('/<session_id>') 
def chat(session_id):
 	token=opentok_sdk.generate_token(session_id)
 	return render_template('chat.html', api_key= api_key, session_id=session_id, token=token)

@app.route('/sendemail')
def email():
 	# Your From email address
	fromEmail = "noreply@mafiawars.com"
	# Recipient
	toEmail = "mvthen@yahoo.com"

	# Create message container - the correct MIME type is multipart/alternative.
	msg = MIMEMultipart('alternative')
	msg['Subject'] = "Mafia Invite!"
	msg['From'] = fromEmail
	msg['To'] = toEmail

	# Create the body of the message (a plain-text and an HTML version).
	# text is your plain-text email
	# html is your html version of the email
	# if the reciever is able to view html emails then only the html
	# email will be displayed
	text = "Hello!\nGet ready to play Mafia Wars!\n"
	html = """\n
	<html>
	  <head></head>
	  <body>
	    Hello there!!!!<br>
	       Your friend has invited you to join a vicious game of Mafia Wars! Please follow the following link to get started! <br>
	    
	  </body>
	</html>
	"""

	# Login credentials
	username = 'mar2260'
	password = "Fhqwghad1!"

	# Record the MIME types of both parts - text/plain and text/html.
	part1 = MIMEText(text, 'plain')
	part2 = MIMEText(html, 'html')

	# Attach parts into message container.
	msg.attach(part1)
	msg.attach(part2)

	# Open a connection to the SendGrid mail server
	s = smtplib.SMTP('smtp.sendgrid.net', 587)

	# Authenticate
	s.login(username, password)

	# sendmail function takes 3 arguments: sender's address, recipient's address
	# and message to send - here it is sent as one string.
	s.sendmail(fromEmail, toEmail, msg.as_string())

	s.quit()



if __name__ == '__main__':
	app.run()
